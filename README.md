# Prerequisites
windows, python installed

# Installing Reuirements
open cmd prompt in administrator mode
>pip install -r requirement.txt

# Configuration setup
### Config.py
vba_filename = [ Binary file Name ] <br />
xlsm_file = [ Macro file with absolute path ] <br />
ingest_file = [ Ingestion files absolute path ] <br />
macro_bin_path = [ Macro Binary folder absolute path ] <br />
completed_path = [ Completed files absolute path ] <br />
macro_name = [ Name of the Macro need to be exceuted in ingest file ] <br />

# Run the Script
open cmd in administrator mode

#For Extracting the Macro,
> python macro_class_utility.py -m extract

#To Ingest the Macro,
> python macro_class_utility.py -m ingest

# Note:
All the Macros are saved as a binary excutable in the bin path.
