import os
import sys
import xlsxwriter
import xlwings as xw
import pandas as pd
import win32com.client 
from zipfile import ZipFile
from zipfile import BadZipfile
import xlwings as xw
from config import *

class macro_utility:
    def __init__(self, vba_filename, xlsm_file, ingest_file, macro_name):  
        self.vba_filename = vba_filename
        self.xlsm_file = xlsm_file
        self.ingest_file = ingest_file
        self.macro_name = macro_name  

    def extract_macro_from_excel(self):
        '''
            Doc_String : Extracting the macro from the excel file
        '''   
        try:
            # Open the Excel xlsm file as a zip file.
            xlsm_zip = ZipFile(self.xlsm_file, 'r')

            # Read the xl/vbaProject.bin file.
            vba_data = xlsm_zip.read('xl/' + 'vbaProject.bin')

            # Write the vba data to a local file.
            vba_file = open(vba_filename, "wb")
            vba_file.write(vba_data)
            vba_file.close()

        except IOError as e:
            print("File error: %s" % str(e))
            exit()

        except KeyError as e:
            # Usually when there isn't a xl/vbaProject.bin member in the file.
            print("File error: %s" % str(e))
            print("File may not be an Excel xlsm macro file: '%s'" % self.xlsm_file)
            exit()

        except BadZipfile as e:
            # Usually if the file is an xls file and not an xlsm file.
            print("File error: %s: '%s'" % (str(e), self.xlsm_file))
            print("File may not be an Excel xlsm macro file.")
            exit()

        except Exception as e:
            # Catch any other exceptions.
            print("File error: %s" % str(e))
            exit()

        print("Extracted: %s" % self.vba_filename)
        

    def ingest_macro_to_excel(self):
        '''
            Doc_String : Ingesting the macro to the excel file
        '''
        try:
            xls_file        = self.ingest_file.split("xlsm")[0] + "xlsx"
            xlsx_name       = os.path.basename(self.ingest_file).split("xlsm")[0] + "xlsx"
            xls_file        = os.path.join(completed_path, xlsx_name)        
            df              = pd.read_excel(self.ingest_file)               
            writer          = pd.ExcelWriter(xls_file, engine='xlsxwriter')
            filename_macro  = ingest_file
            workbook        = writer.book
            workbook.filename = filename_macro
            workbook.add_vba_project(os.path.join(macro_bin_path,self.vba_filename))
            df.to_excel(writer, index=False)
            writer.save()

            if os.path.exists(ingest_file):
                xl = win32com.client.Dispatch('Excel.Application')
                xl.Workbooks.Open(Filename = self.ingest_file)
                xl.Application.Run("Macro3")            
                xl.Application.Quit()
                del xl
        except Exception as e:
            print(str(e))


if __name__ == "__main__":    
    n = len(sys.argv)
    x = macro_utility(vba_filename, xlsm_file, ingest_file, macro_name)
    if sys.argv:
        if len(sys.argv) == 3 and sys.argv[1] == "-m":
            if sys.argv[2] == "extract":                
                x.extract_macro_from_excel()
            elif sys.argv[2] == "ingest":
                x.ingest_macro_to_excel()
            else:
                print("please enter the activity name like extract or ingest")
        else:
            print("Command line parameter are incorrect. Please validate it")
    
    